package com.puravida.log4asciidoc;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class AsciidocConstants {

    public static final String MDC_FILE = "MDC_FILE";

    public static final String MARKER_PATH = "ADOC_";
    public static final Marker MARKER_TITLE = MarkerFactory.getMarker(MARKER_PATH+"TITLE");
    public static final Marker MARKER_SECTION = MarkerFactory.getMarker(MARKER_PATH+"SECTION");

}
