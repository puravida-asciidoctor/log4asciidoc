package com.puravida.log4asciidoc;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import org.slf4j.Marker;

import java.io.*;

public class AsciidocAppender extends AppenderBase<ILoggingEvent> {

    @Override
    protected void append(ILoggingEvent eventObject) {
        if( !eventObject.getMDCPropertyMap().containsKey(AsciidocConstants.MDC_FILE) ){
            return;
        }
        Marker marker = eventObject.getMarker();
        boolean append =  !(marker != null && marker.getName() == AsciidocConstants.MARKER_TITLE.getName());
        try (FileWriter f = new FileWriter(eventObject.getMDCPropertyMap().get(AsciidocConstants.MDC_FILE), append);
             BufferedWriter b = new BufferedWriter(f);
             PrintWriter p = new PrintWriter(b);) {
            if( !append){
                p.println("= "+eventObject.getFormattedMessage());
                p.println("");
            }else {
                if( marker != null && marker.getName() == AsciidocConstants.MARKER_SECTION.getName()){
                    p.println("== "+eventObject.getFormattedMessage());
                    p.println("");
                }else {
                    p.println(eventObject.getFormattedMessage());
                    p.println("");
                }
            }
            p.flush();
        }catch(Exception e){
        }
    }
}
